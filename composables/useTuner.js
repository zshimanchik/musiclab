import { ref, isRef, onUnmounted } from 'vue'
import analyseAudioData from '@/audio-processor.js'


const MIN_FREQ = 32
const MAX_FREQ = 2000
let SAMPLE_RATE = 44100

export default function useTuner(algorithm = 'AMDF', callback = null) {
    const detectedResult = ref({
        frequency: null,
        octave: null,
        key: null,
        correctHz: null,
        centsOff: null,
        halfSteps: null,
    })
    const processTime = ref(null)
    const isRunning = ref(false)

    let audioStream = null
    let audioProcessor = null
    let audioContext = null

    async function start() {
        audioContext = new AudioContext()
        SAMPLE_RATE = audioContext.sampleRate
        isRunning.value = true
        audioStream = await navigator.mediaDevices.getUserMedia({
            audio: {
                channelCount: 1,
                sampleRate: SAMPLE_RATE,
            },
        })

        audioProcessor = audioContext.createScriptProcessor(4096, 1, 1)
        audioProcessor.onaudioprocess = onAudioProcess

        const source = audioContext.createMediaStreamSource(audioStream)
        source.connect(audioProcessor)
        audioProcessor.connect(audioContext.destination)
    }

    function stop() {
        if (audioProcessor) {
            audioProcessor.disconnect()
            audioProcessor.onaudioprocess = null
            audioProcessor = null
        }
        if (audioStream) {
            audioStream.getTracks().forEach((track) => track.stop())
            audioStream = null
        }
        if (audioContext) {
            audioContext.close()
            audioContext = null
        }
        isRunning.value = false
    }

    function toggle() {
        if (isRunning.value) {
            stop()
        } else {
            start()
        }
    }

    function onAudioProcess(event) {
        const startTime = Date.now()
        const audioData = event.inputBuffer.getChannelData(0)
        // audioData is a Float32Array
        let result = analyseAudioData({
            a4: 440,
            sampleRate: SAMPLE_RATE,
            audioData,
            accidentals: 'sharps',
            algorithm: isRef(algorithm) ? algorithm.value : algorithm,
        })
        // console.debug('Analyzed data:', result)
        if (result?.frequency > MAX_FREQ || result?.frequency < MIN_FREQ) {
            result = null
        }
        detectedResult.value = result
        if (callback) {
            callback(result)
        }

        // console.debug('processing data done')
        processTime.value = Date.now() - startTime
    }

    onUnmounted(() => { stop() })

    return { detectedResult, processTime, start, stop, toggle, isRunning }
}