import { ref, computed } from 'vue'
import music from '@/music'
import { emitter } from '@/emitter'


export default function useMidi() {
    const status = ref('not initialized')
    const connected = computed(() => status.value === 'connected')
    const inputs = ref([])

    function start() {
        console.log('initializing midi')
        status.value = 'initializing'
        if (navigator.requestMIDIAccess) {
            navigator.requestMIDIAccess().then(onMidiSuccessInit, onMidiReject)
        } else {
            status.value = "Browser doesn't support or not allow"
        }
    }

    function onMidiSuccessInit(midi) {
        if (midi.inputs.size === 0) {
            status.value = 'No Midi input devices present'
            return
        }
        console.log(midi.inputs)
        // Hook the message handler for all MIDI inputs

        const newInputs = []
        midi.inputs.forEach((input) => {
            console.log('assigning event handler for input ', input)
            input.onmidimessage = MIDIMessageEventHandler
            newInputs.push(input)
        })
        inputs.value = newInputs
        status.value = 'connected'
    }
    function onMidiReject(err) {
        status.value = 'Midi access was rejected: ' + err
    }
    function MIDIMessageEventHandler(event) {
        if ((event.data[0] & 0xf0) === 0x90) {
            // some magic stuff
            if (event.data[2] !== 0) {
                // if velocity != 0, this is a note-on message
                const note = music.midiPitchToNote(event.data[1])
                noteOn(note)
            } else {
                const note = music.midiPitchToNote(event.data[1])
                noteOff(note)
            }
        }
    }
    function noteOn(note) {
        emitter.emit('midiNoteOn', note)
    }
    function noteOff(note) {
        emitter.emit('midiNoteOff', note)
    }
    function stop() {
        inputs.value.forEach((input) => {
            console.log('disconnecting', input)
            input.onmidimessage = null
        })
        inputs.value = []
        status.value = 'disconnected'
    }

    function toggle() {
        connected.value ? stop() : start()
    }

    return { start, stop, toggle, status, connected, inputs }
}
