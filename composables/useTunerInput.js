import useTuner from '~/composables/useTuner'
import { emitter } from '@/emitter'


export default function useTunerInput(noteDuration = 3000) {
    const timeoutIds = {}
    function tunerCallback(detectedResult) {
        if (!detectedResult) {
            return
        }
        const note = `${detectedResult.key}${detectedResult.octave}`

        if (!timeoutIds[note]) {
            emitter.emit('midiNoteOn', note)
            timeoutIds[note] = setTimeout(() => {
                emitter.emit('midiNoteOff', note)
                timeoutIds[note] = null
            }, noteDuration)
        }
    }

    const tuner = useTuner('AMDF', tunerCallback)

    return { start: tuner.start, stop: tuner.stop, toggle: tuner.toggle, isRunning: tuner.isRunning }
}
