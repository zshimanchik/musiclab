
export const state = () => ({
    tune: ['A4', 'E4', 'C4', 'G4'],
})

export const mutations = {
    SET_TUNE(state, tune) {
        state.tune = tune
    },
    RESET(state) {
        state.tune = ['A4', 'E4', 'C4', 'G4']
    },
}
